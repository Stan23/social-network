package socialnetwork.controllers.RestAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import socialnetwork.exceptions.DuplicateUsernameException;
import socialnetwork.models.User;
import socialnetwork.services.contracts.PostService;
import socialnetwork.services.contracts.UserService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/users")
public class UserControllerREST {

    private UserService userService;
    private PostService postService;

    @Autowired
    public UserControllerREST(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @GetMapping
    public List<User> userList() {
        return userService.findAll();
    }

    @GetMapping("/active")
    public List<User> showActiveUsers() {
        return userService.findAllActivated();
    }

    @GetMapping("/{username}")
    public User getByUserName(@PathVariable String username) {
        try {
            return userService.findByUserName(username);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with username %s does not exits.", username));
        }
    }

    @PostMapping
    public User create(@RequestBody @Valid User user) throws DuplicateUsernameException {
        try {
            userService.createUser(user);
            return user;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{username}")
    public void delete(@PathVariable String username) {
        try {
            userService.deleteUser(username);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{username}")
    public User update(
            @PathVariable String username,
            @RequestBody @Valid User user) {
        try {
            userService.updateUser(user);
            return user;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
