package socialnetwork.exceptions;

public class DuplicateUsernameException extends Exception{
    public DuplicateUsernameException() {
        super("Username is already taken!");
    }
}
