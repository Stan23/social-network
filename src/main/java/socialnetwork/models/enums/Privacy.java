package socialnetwork.models.enums;


public enum Privacy {
    PUBLIC("Public"),
    BUDDIES_ONLY("Buddies only"),
    PRIVATE("Private");

    private String displayName;

    Privacy(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
