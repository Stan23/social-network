package socialnetwork.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import socialnetwork.models.Comment;
import socialnetwork.models.Post;
import socialnetwork.models.User;
import socialnetwork.repositories.CommentRepository;
import socialnetwork.repositories.PostRepository;
import socialnetwork.repositories.UserRepository;
import socialnetwork.services.contracts.PostService;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private UserRepository userRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    @Override
    public Post createPost(Post post, String userName) {
        User user = userRepository.findByUserName(userName);
        if (user == null) {
            throw new EntityNotFoundException("Non-existent users can't create posts =)");
        }
        post.setUser(user);
        return postRepository.save(post);
    }
    
    @Transactional
    @Override
    public Post updatePost(Post post, String text, MultipartFile picture) throws IOException {
            Post postToUpdate = findPostByID(post.getPostID());
            postToUpdate.setText(text);
            postToUpdate.setPicture(picture);
            return postRepository.save(postToUpdate);
    }

    @Override
    public void removePost(int id) {
        postRepository.deleteById(id);
    }

    @Override
    public Post findPostByID(int postID){
        return postRepository.findById(postID).orElseThrow(() -> new EntityNotFoundException(
                String.format("Post with id %d does not exist.", postID)));
    }

    @Override
    public List<Post> getUserPosts(User user) {
        return postRepository.findAllByUserUsernameOrderByPostTimeDesc(user.getUsername());
    }

    @Override
    public List<Comment> getPostComments(Post post) {
        return post.getComments();
    }

    @Override
    public void likePost(String userName, Post post) {
        User user = userRepository.findByUserName(userName);
        if (post.getLikes().contains(user)){
            post.getLikes().remove(user);
        }else{
            post.getLikes().add(user);
        }
        postRepository.save(post);
    }

    @Override
    public int getNumberOfLikes(int postID) {
        return findPostByID(postID).getLikes().size();
    }

    @Override
    public List<Post> getUserFeed(User user) {
        List<Post> userFeed = new ArrayList<>();
        for (User friend:user.getFriends())
        {
            userFeed.addAll(friend.getPosts());
        }
        Collections.sort(userFeed, (p1, p2) -> p2.getPostTime().compareTo(p1.getPostTime()));
        return userFeed;
    }

    @Override
    public List<Post> getPublicFeed() {
        return postRepository.getPublicFeed();
    }
}