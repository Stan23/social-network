package socialnetwork.repositories;

import org.springframework.data.repository.CrudRepository;
import socialnetwork.models.Comment;

public interface CommentRepository extends CrudRepository<Comment, Integer> {
}
